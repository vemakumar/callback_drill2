const boards = require('./data/boards_1.json')

/* 
	Problem 1: Write a function that will return a particular board's information
    based on the boardID that is passed from the given list of boards in boards.json 
    and then pass control back to the code that called it by using a callback function.
*/


function getBoardInfo (boards,boardID, callback) {
  setTimeout(() => {
      try {
      const board = boards.find(board => board.id === boardID)
      if (!board) {
        callback(new Error('not found'), null)
        return
      }
      callback(null, board)
    } catch (err) {
      callback(err, null)
    }
  }, 2 * 1000)
}

module.exports=getBoardInfo