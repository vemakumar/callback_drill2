/* 
	Problem 2:- Write a function that will return all lists that belong to a board
    based on the boardID that is passed to it from the given data in lists.json.
    Then pass control back to the code that called it by using a callback function.
*/

const boards = require('./data/boards_1.json')
const lists = require('./data/lists_1.json')

function listForBoard (boardId, lists, callback1) {
  setTimeout(() => {
    try {
      if (Object.hasOwnProperty.call(lists, boardId)) {
        callback1(null, lists[boardId])
      }
      else{
        throw new Error("Not Found")
      }
      
    } catch (err) {
      throw new Error(err.message)
    }
  }, 2000)
}


module.exports = listForBoard