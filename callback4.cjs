/* 
	Problem 4: Write a function that will use the previously written functions to get the following 
    information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const lists = require('./data/lists_1.json')
const boards = require('./data/boards_1.json')
const cards = require("./data/cards_1.json")


const getBoardInfo = require('./callback1.cjs')
const listsForBoard = require('./callback2.cjs')
const cardDetailsWithListId = require('./callback3.cjs')

const boardID = 'mcu453ed'

function getThanosInfo () {
  getBoardInfo(boards, boardID, (err, board) => {
    if (err) {
      console.error(err.message)
      return
    }
    const boardId = board.id
    console.log('board : ',boardId)
    listsForBoard(boardId, lists, (err, listOfBoard) => {
      if (err) {
        console.log(err)
      }
      console.log('list board : ',listOfBoard)
      const listDetails = listOfBoard.filter(ele => ele.name === 'Mind')
      cardDetailsWithListId(listDetails[0].id, cards, (err, res) => {
        if (err) {
          console.log(err)
        }
        console.log('Mind list : ',res)
      })
    })
  })
}

module.exports=getThanosInfo