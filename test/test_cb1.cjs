const getBoardInfo=require("../callback1.cjs")
const boards = require('../data/boards_1.json')
const boardID = 'mcu453ed' 

getBoardInfo(boards,boardID, (err, board) => {
  if (err) {
    console.error(err.message)
    return
  }
  console.log(board)
  return board
})